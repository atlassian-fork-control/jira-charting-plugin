package it.com.atlassian.jira.ext.charting;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;

public abstract class AbstractChartTestCase extends BaseJiraFuncTest
{
    public static final String ISSUE_TYPE_BUG = "Bug";
    public static final String ISSUE_TYPE_NEWFEATURE = "New Feature";
    public static final String ISSUE_TYPE_IMPROVEMENT = "Improvement";

    public static final String TEST_PROJECT_KEY = "TST";

    public static final String TEST_PROJECT_NAME = "Test Project";

    static final String TEST_PROJECT_ID = "10100";

    static final String TEST_PROJECT_PROJECT_FILTER_DESIGNATION = "project-" + TEST_PROJECT_ID;

    static final String TEST_USERNAME = "john.doe";

    protected void selectProjectOrFilterId(final String projectOrFilterDesignation)
    {
        if (!projectOrFilterDesignation.matches("(project|filter)-\\d+"))
        {
            throw new IllegalArgumentException("the project or filter designation is not in a valid format");
        }
        tester.setFormElement("projectOrFilterId", projectOrFilterDesignation);
    }

    protected void assertChartImagePresent()
    {
        tester.assertTextPresent("<img src='data:image/png;base64,");
    }
}
