package com.atlassian.jira.ext.charting.gadgets;

import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.bc.issue.worklog.TimeTrackingConfiguration;
import com.atlassian.jira.charts.Chart;
import com.atlassian.jira.charts.util.ChartUtils;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.ext.charting.gadgets.charts.WorkloadPieChart;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.search.ReaderCache;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.util.JiraDurationUtils;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.FieldVisibilityManager;
import junit.framework.TestCase;
import org.jfree.chart.urls.CategoryURLGenerator;
import org.jfree.data.category.CategoryDataset;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.jira.ext.charting.gadgets.charts.ChartParamKeys.KEY_COMPLETE_DATASET;
import static com.atlassian.jira.ext.charting.gadgets.charts.ChartParamKeys.KEY_COMPLETE_DATASET_URL_GENERATOR;
import static org.mockito.Mockito.*;

public class WorkloadPieChartResourceTestCase extends TestCase
{
    private WorkloadPieChartResource workloadPieChartResource;

    public static final String KEY_TOTAL_WORK_HOURS = "numIssues";

    private Map<String, Object> parameters;

    private String projectName = "Project Fate";
    private String projectOrFilterId;
    private String statisticType = "assignees";
    private String issueTimeType = "timespent";
    private String location = "/confluence";
    private String imageMap = "/imageMap";
    private String imageMapName = "/imageMapName";


    private int width = 800;
    private int height = 600;

    private boolean returnData;

    private long projectNumber = 1987;
    private long workHours = 2150;
    private long filterId = 2050;

    @Mock private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock private ProjectManager projectManager;
    @Mock private SearchRequestService searchRequestService;
    @Mock private SearchService searchService;
    @Mock private ChartUtils chartUtils;
    @Mock private PermissionManager permissionManager;
    @Mock private VelocityRequestContextFactory velocityRequestContextFactory;
    @Mock private ApplicationProperties applicationProperties;
    @Mock private CustomFieldManager customFieldManager;
    @Mock private FieldManager fieldManager;
    @Mock private ConstantsManager constantsManager;
    @Mock private IssueIndexManager issueIndexManager;
    @Mock private SearchProvider searchProvider;
    @Mock private FieldVisibilityManager fieldVisibilityManager;
    @Mock private ReaderCache readerCache;
    @Mock private TimeTrackingConfiguration timeTrackingConfiguration;
    @Mock private SearchRequest searchRequest;
    @Mock private WorkloadPieChart workloadPieChart;
    @Mock private Chart theChart;
    @Mock private JiraDurationUtils jiraDurationUtils;
    @Mock private Project aProject;
    @Mock private CategoryURLGenerator completeUrlGenerator;
    @Mock private CategoryDataset completeDataset;
    @Mock private JiraServiceContextImpl jiraServiceContextImpl;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        MockitoAnnotations.initMocks(this);

        parameters = new HashMap<String, Object>();

        parameters.put(KEY_TOTAL_WORK_HOURS, workHours);
        parameters.put(KEY_COMPLETE_DATASET_URL_GENERATOR, completeUrlGenerator);
        parameters.put(KEY_COMPLETE_DATASET, completeDataset);

        when(applicationProperties.getOption(APKeys.JIRA_OPTION_TIMETRACKING)).thenReturn(true);

        when(workloadPieChart.generate(jiraAuthenticationContext, searchRequest, statisticType, issueTimeType, width, height)).thenReturn(theChart);

        when(theChart.getParameters()).thenReturn(parameters);
        when(theChart.getLocation()).thenReturn(location);
        when(theChart.getImageMap()).thenReturn(imageMap);
        when(theChart.getImageMapName()).thenReturn(imageMapName);

        workloadPieChartResource = new WorkloadPieChartResource(jiraAuthenticationContext,
                projectManager, searchRequestService, searchService, chartUtils,
                permissionManager, velocityRequestContextFactory, applicationProperties,
                customFieldManager, fieldManager, constantsManager, issueIndexManager, searchProvider,
                fieldVisibilityManager, readerCache)
        {
            @Override
            protected SearchRequest getSearchRequest(String projectOrFilterId,
                    Map<String, Object> params)
            {
                return searchRequest;
            }

            @Override
            protected WorkloadPieChart getWorkloadPieChart()
            {
                return workloadPieChart;
            }

            @Override
            protected JiraDurationUtils getJiraDurationUtils()
            {
                return jiraDurationUtils;
            }

            @Override
            protected JiraServiceContextImpl getJiraServiceContext()
            {
                return jiraServiceContextImpl;
            }
        };
    }

    public void testGenerateResponseWithoutReturnData() throws SearchException, IOException
    {
        returnData = false;

        projectOrFilterId = "project-" + projectNumber;

        when(projectManager.getProjectObj(projectNumber)).thenReturn(aProject);
        when(aProject.getName()).thenReturn(projectName);

        Response response = workloadPieChartResource.generate(projectOrFilterId,
                statisticType, issueTimeType, width, height, returnData, false);

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        verify(jiraDurationUtils, times(1)).getShortFormattedDuration(anyLong());
    }

    public void testGenerateResponseWithReturnData() throws SearchException, IOException
    {
        String filterTitle = "My Fav Filter";

        returnData = true;

        projectOrFilterId = "filter-" + filterId;

        when(searchRequestService.getFilter(jiraServiceContextImpl, filterId)).thenReturn(searchRequest);
        when(searchRequest.getName()).thenReturn(filterTitle);

        Response response = workloadPieChartResource.generate(projectOrFilterId,
                statisticType, issueTimeType, width, height, returnData, false);

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        verify(jiraAuthenticationContext, never()).getI18nHelper();
    }
}
