package com.atlassian.jira.ext.charting.data;

import com.atlassian.jira.charts.util.LuceneDateUtils;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.statistics.util.FieldDocumentHitCollector;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.util.EasyList;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.lucene.document.DateTools;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.document.StringField;
import org.jfree.data.time.Day;
import org.jfree.data.time.RegularTimePeriod;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.ofbiz.core.entity.DelegatorInterface;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import static org.mockito.Mockito.when;

public class TestAverageTimeInStatusHitCollector extends TestCase {

    private static final Logger logger = Logger.getLogger(TestAverageTimeInStatusHitCollector.class);

    private static final Double VALUE_TO_RETURN_FOR_TIME_IN_SECONDS = 2d;

    private static final Double VALUE_TO_RETURN_FOR_NUMBER_OF_TIMES_IN_STATUS = 1d;

    @Mock
    private ConstantsManager constantsManager;
    @Mock
    private Status status;
    private TimeZone timeZone = TimeZone.getDefault();

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        MockitoAnnotations.initMocks(this);
    }

    protected FieldDocumentHitCollector getDocumentHitCollector(
            String resolutionDateConstant,
            String averageTimeInStatusConstant,
            String status,
            Map<RegularTimePeriod, Double> totalTime,
            Map<RegularTimePeriod, Double> totalCount,
            Class<Day> timePeriodClass,
            int days,
            boolean numberTimesInStatus) {
        return new AverageTimeInStatusHitCollector(
                resolutionDateConstant,
                averageTimeInStatusConstant,
                status,
                totalTime,
                totalCount,
                timePeriodClass,
                days,
                numberTimesInStatus, timeZone) {

            protected Double getSecondsInStatus(String amountOfTimeString) {
                return VALUE_TO_RETURN_FOR_TIME_IN_SECONDS;
            }

            protected Double getNumberOfTimesInStatus(String amountOfTimeString) {
                return VALUE_TO_RETURN_FOR_NUMBER_OF_TIMES_IN_STATUS;
            }
        };
    }

    public void testCollectTimeInStatus() throws Exception {
        final String averageTimeInStatusConstant = "averageTimeInStatusConstant";
        final String resolutionDateConstant = "resolutionDateConstant";
        final String customFieldValue = "foo";
        final Map<RegularTimePeriod, Double> totalTime = new LinkedHashMap<>();
        final Map<RegularTimePeriod, Double> totalCount = new LinkedHashMap<>();

        Calendar now = Calendar.getInstance();

        final List<Document> docs = new ArrayList<>();
        Document doc;
        now.add(Calendar.DAY_OF_YEAR, -1);
        doc = new Document();
        doc.add(new StringField(averageTimeInStatusConstant, customFieldValue, Field.Store.YES));
        doc.add(new LongPoint(resolutionDateConstant, DateTools.round(now.getTimeInMillis(), DateTools.Resolution.SECOND)));
        docs.add(doc);

        now.add(Calendar.DAY_OF_YEAR, -1);
        doc = new Document();
        doc.add(new StringField(averageTimeInStatusConstant, customFieldValue, Field.Store.YES));
        doc.add(new LongPoint(resolutionDateConstant, DateTools.round(now.getTimeInMillis(), DateTools.Resolution.SECOND)));
        docs.add(doc);

        FieldDocumentHitCollector docHitCollector = getDocumentHitCollector(
                resolutionDateConstant,
                averageTimeInStatusConstant,
                String.valueOf("Open".hashCode()),
                totalTime,
                totalCount,
                Day.class,
                3,
                true);

        when(status.getName()).thenReturn("Open");
        when(status.getId()).thenReturn(String.valueOf("Open".hashCode()));
        when(constantsManager.getStatusObjects()).thenReturn(EasyList.build(status));
        TimeInStatusDAO.setConstantsManager(constantsManager);

        for (final Iterator<Document> i = docs.iterator(); i.hasNext(); ) {
            docHitCollector.collect(i.next());
        }

        assertEquals(4, totalCount.size());
        assertEquals(1d,
                totalCount.get(
                        RegularTimePeriod.createInstance(
                                Day.class,
                                LuceneDateUtils.indexableFieldToDate(docs.get(0).getField(resolutionDateConstant)
                                ),
                                TimeZone.getDefault()
                        )
                )
        );
        assertEquals(1d,
                totalCount.get(
                        RegularTimePeriod.createInstance(
                                Day.class,
                                LuceneDateUtils.indexableFieldToDate(docs.get(1).getField(resolutionDateConstant)
                                ),
                                TimeZone.getDefault()
                        )
                )
        );

        totalTime.clear();
        totalCount.clear();

        docHitCollector = getDocumentHitCollector(
                resolutionDateConstant,
                averageTimeInStatusConstant,
                String.valueOf("Open".hashCode()),
                totalTime,
                totalCount,
                Day.class,
                3,
                false); /* For counting time instead of freq */

        for (final Iterator<Document> i = docs.iterator(); i.hasNext(); ) {
            docHitCollector.collect(i.next());
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Dumping output of totalTime map");

            for (final Iterator<?> i = totalTime.entrySet().iterator(); i.hasNext(); ) {
                final Map.Entry e = (Map.Entry) i.next();

                logger.debug("Key: " + e.getKey() + ";Value: " + e.getValue());
            }
        }

        assertEquals(4, totalTime.size());
        assertEquals(2d,
                totalTime.get(
                        RegularTimePeriod.createInstance(
                                Day.class,
                                LuceneDateUtils.indexableFieldToDate(docs.get(0).getField(resolutionDateConstant)
                                ),
                                TimeZone.getDefault()
                        )
                )
        );
        assertEquals(2d,
                totalTime.get(
                        RegularTimePeriod.createInstance(
                                Day.class,
                                LuceneDateUtils.indexableFieldToDate(docs.get(1).getField(resolutionDateConstant)
                                ),
                                TimeZone.getDefault()
                        )
                )
        );
    }

    protected static class TimeInStatusDAO extends com.atlassian.jira.ext.charting.field.TimeInStatusDAO {

        public TimeInStatusDAO(final DelegatorInterface delegatorInterface) {
            super(delegatorInterface);
        }

        public static void setConstantsManager(final ConstantsManager _constantsManager) {
            TimeInStatusDAO.constantsManager = _constantsManager;
        }
    }
}
