package com.atlassian.jira.ext.charting.gadgets;

import static org.mockito.Mockito.when;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.charts.ChartFactory;
import com.atlassian.jira.charts.util.ChartUtils;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.rest.v1.model.errors.ErrorCollection;
import com.atlassian.jira.rest.v1.model.errors.ValidationError;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.query.Query;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Vector;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

public class ProjectOrFilterIdBasedChartResourceTestcase extends TestCase
{
    private ProjectOrFilterIdBasedChartResource projectOrFilterIdBasedChartResource;

    private Collection<ValidationError> validationErrors;

    private String projectOrFilterId;
    private MessageSet messageSet;

    private static String MAX_DAYS_AP_PREFIX = "jira.chart.days.previous.limit.";

    @Mock private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock private ProjectManager projectManager;
    @Mock private SearchRequestService searchRequestService;
    @Mock private SearchService searchService;
    @Mock private ChartUtils chartUtils;
    @Mock private PermissionManager permissionManager;
    @Mock private VelocityRequestContextFactory velocityRequestContextFactory;
    @Mock private ApplicationProperties applicationProperties;
    @Mock private SearchRequest searchRequest;
    @Mock private Query query;
    @Mock private I18nHelper i18nHelper;
    @Mock private VelocityRequestContext velocityRequestContext;
    @Mock private ApplicationUser applicationUser;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        MockitoAnnotations.initMocks(this);

        validationErrors = new Vector<ValidationError>();

        messageSet = new MessageSetImpl();

        projectOrFilterIdBasedChartResource = new ProjectOrFilterIdBasedChartResource(jiraAuthenticationContext,
                projectManager, searchRequestService, searchService, chartUtils, permissionManager,
                velocityRequestContextFactory, applicationProperties);
    }

    public void testValidateProjectOrFilterIdWithSearchRequestAndNoError()
    {
        setConditions(messageSet);

        assertTrue(validationErrors.isEmpty());
    }

    public void testValidateProjectOrFilterIdWithSearchRequestAndWithError()
    {
        String errorMessage = "Ohhhh NOOOO!!!";

        messageSet.addErrorMessage(errorMessage);

        setConditions(messageSet);

        assertCheck("gadget.common.invalid.filter.validationfailed");
    }

    public void testValidateEmptyProjectOrFilterId()
    {
        projectOrFilterId = "";

        projectOrFilterIdBasedChartResource.validateProjectOrFilterId(projectOrFilterId, validationErrors);

        assertCheck("gadget.common.required.query");
    }

    public void testValidateProjectOrFilterIdWithNoSearchRequest()
    {
        projectOrFilterId = "Project Fate";

        projectOrFilterIdBasedChartResource.validateProjectOrFilterId(projectOrFilterId, validationErrors);

        assertCheck("gadget.common.required.query");
    }

    public void testValidateDaysAgainstPeriod()
    {
        String daysPrevious = "10";

        String maxDaysPropertyKey = MAX_DAYS_AP_PREFIX + ChartFactory.PeriodName.daily.toString();

        when(applicationProperties.getDefaultBackedString(maxDaysPropertyKey)).thenReturn("0");

        projectOrFilterIdBasedChartResource.validateDaysPrevious(daysPrevious, validationErrors);

        assertCheck("gadget.common.days.overlimit.for.period");
    }

     private void assertCheck(String error)
    {
        assertTrue(!validationErrors.isEmpty());
        assertEquals(error, ((ValidationError)validationErrors.iterator().next()).getError());
    }

    private void setConditions(MessageSet messageSet)
    {
        projectOrFilterId = "Project Fate";

        when(chartUtils.retrieveOrMakeSearchRequest(projectOrFilterId, new HashMap<String, Object>())).thenReturn(searchRequest);
        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(applicationUser);
        when(searchRequest.getQuery()).thenReturn(query);
        when(searchService.validateQuery(applicationUser, query)).thenReturn(messageSet);

        projectOrFilterIdBasedChartResource.validateProjectOrFilterId(projectOrFilterId, validationErrors);
    }
}
