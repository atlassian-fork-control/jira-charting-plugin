package com.atlassian.jira.ext.charting.data;

import com.atlassian.jira.charts.util.LuceneDateUtils;
import com.atlassian.jira.issue.statistics.util.FieldDocumentHitCollector;
import com.google.common.collect.Sets;
import org.apache.lucene.document.Document;
import org.jfree.data.time.RegularTimePeriod;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

public class DateRangeObjectHitCollector extends FieldDocumentHitCollector
{
    private final String dateDocumentConstant1;

    private final String dateDocumentConstant2;

    private final Map<RegularTimePeriod, List<Long>> result;

    private final Class timePeriodClass;

    private final TimeZone userTimeZone;

    public DateRangeObjectHitCollector(String dateDocumentConstant1, String dateDocumentConstant2, Map<RegularTimePeriod, List<Long>> result, Class timePeriodClass, TimeZone userTimeZone)  {
        this.dateDocumentConstant1 = dateDocumentConstant1;
        this.dateDocumentConstant2 = dateDocumentConstant2;
        this.result = result;
        this.timePeriodClass = timePeriodClass;
        this.userTimeZone = userTimeZone;
    }

    @Override
    protected Set<String> getFieldsToLoad() {
        return Collections.unmodifiableSet(Sets.newHashSet(dateDocumentConstant1, dateDocumentConstant2));
    }

    @Override
    public void collect(Document d)
    {
        Date creationDate = LuceneDateUtils.indexableFieldToDate(d.getField(dateDocumentConstant1));
        Date otherDate = LuceneDateUtils.indexableFieldToDate(d.getField(dateDocumentConstant2));

        RegularTimePeriod period = RegularTimePeriod.createInstance(timePeriodClass, otherDate, userTimeZone);

        List<Long> values = result.get(period);
        if (values == null)
            values = new ArrayList<Long>();

        values.add(otherDate.getTime() - creationDate.getTime());
        result.put(period, values);
    }
}
