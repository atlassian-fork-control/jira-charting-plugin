package com.atlassian.jira.ext.charting.gadgets;

import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.charts.jfreechart.TimePeriodUtils;
import com.atlassian.jira.charts.util.ChartUtils;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.rest.v1.util.CacheControl;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;

import org.apache.commons.lang.StringUtils;
import org.jfree.chart.urls.XYURLGenerator;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.time.TimeSeriesDataItem;
import org.jfree.data.xy.XYDataset;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import static com.atlassian.jira.ext.charting.gadgets.charts.ChartParamKeys.KEY_COMPLETE_DATASET;
import static com.atlassian.jira.ext.charting.gadgets.charts.ChartParamKeys.KEY_COMPLETE_DATASET_URL_GENERATOR;


public class AverageStatusChartResource extends ProjectOrFilterIdBasedChartResource
{

    final CustomFieldManager customFieldManager;

    final ConstantsManager constantsManager;

    final IssueIndexManager issueIndexManager;

    final SearchProvider searchProvider;

    final TimeZoneManager timeZoneManager;

    public AverageStatusChartResource(JiraAuthenticationContext jiraAuthenticationContext, ProjectManager projectManager, SearchRequestService searchRequestService, SearchService searchService, ChartUtils chartUtils, PermissionManager permissionManager, VelocityRequestContextFactory velocityRequestContextFactory, ApplicationProperties applicationProperties, CustomFieldManager customFieldManager, ConstantsManager constantsManager, IssueIndexManager issueIndexManager, SearchProvider searchProvider, TimeZoneManager timeZoneManager)
    {
        super(jiraAuthenticationContext, projectManager, searchRequestService, searchService, chartUtils, permissionManager, velocityRequestContextFactory, applicationProperties);
        this.customFieldManager = customFieldManager;
        this.constantsManager = constantsManager;
        this.issueIndexManager = issueIndexManager;
        this.searchProvider = searchProvider;
        this.timeZoneManager = timeZoneManager;
    }

    @GET
    @Path("config/statuses")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getStatuses()
    {
        return Response.ok(getStatusObjectsAsStatuses()).cacheControl(CacheControl.NO_CACHE).build();
    }

    Statuses getStatusObjectsAsStatuses()
    {
        List<Status> statusList = new ArrayList<Status>();

        for (com.atlassian.jira.issue.status.Status status : constantsManager.getStatusObjects())
        {
            Status aStatus = new Status();
            aStatus.label = status.getName();
            aStatus.value = status.getId();

            statusList.add(aStatus);
        }

        Statuses statuses = new Statuses();
        statuses.statuses = statusList;
        return statuses;
    }

    @XmlRootElement
    @XmlType(namespace = "com.atlassian.jira.ext.charting")
    public static class Status
    {
        @XmlElement
        private String label;

        @XmlElement
        private String value;
    }

    @XmlRootElement
    @XmlType(namespace = "com.atlassian.jira.ext.charting")
    public static class Statuses
    {
        @XmlElement
        private List<Status> statuses;
    }

    @XmlRootElement
    public static class AverageStatusChart
    {
        @XmlElement
        String location;

        @XmlElement
        String imageMap;

        @XmlElement
        String imageMapName;

        @XmlElement
        int width;

        @XmlElement
        int height;

        @XmlElement
        String filterTitle;

        @XmlElement
        String filterUrl;

        @XmlElement
        String daysprevious;

        @XmlElement
        List<String> selectedStatusNames;

        @XmlElement
        List<AverageStatusDataRow> data;

        @XmlElement
        String base64Image;

// Empty method/constructor need not be calculated by Clover
///CLOVER:OFF
        public AverageStatusChart()
        {
        }
///CLOVER:ON

        public AverageStatusChart(String location, String imageMap, String imageMapName, int width, int height, String filterTitle, String filterUrl, String daysprevious, String base64Image)
        {
            this.location = location;
            this.imageMap = imageMap;
            this.imageMapName = imageMapName;
            this.width = width;
            this.height = height;
            this.filterTitle = filterTitle;
            this.filterUrl = filterUrl;
            this.daysprevious = daysprevious;
            this.base64Image = base64Image;
        }
    }

    List<String> getSelectedStatuses(Map<String, Object> chartParams)
    {
        List<String> selectedStatuses = new ArrayList<String>();
        XYDataset completeDataset = (XYDataset) chartParams.get(KEY_COMPLETE_DATASET);

        for (int i = 0; i < completeDataset.getSeriesCount(); ++i)
            selectedStatuses.add(completeDataset.getSeriesKey(i).toString());

        Collections.sort(selectedStatuses);
        return selectedStatuses;
    }

    List<AverageStatusDataRow> getData(Map<String, Object> chartParams)
    {
        XYURLGenerator completeUrlGenerator = (XYURLGenerator) chartParams.get(KEY_COMPLETE_DATASET_URL_GENERATOR);
        XYDataset completeDataset = (XYDataset) chartParams.get(KEY_COMPLETE_DATASET);
        return generateDataSet(completeDataset, completeUrlGenerator);
    }

    List<AverageStatusDataRow> generateDataSet(XYDataset dataset, XYURLGenerator urlGenerator)
    {
        TimeSeriesCollection timeSeries = (TimeSeriesCollection) dataset;

        Map<Number, AverageStatusDataRow> dataRowMap = new LinkedHashMap<Number, AverageStatusDataRow>();
        TimePeriodUtils timePeriodUtils = new TimePeriodUtils(timeZoneManager);

        for (int seriesIndex = 0, seriesCount = timeSeries.getSeriesCount(); seriesIndex < seriesCount; ++seriesIndex)
        {
            Comparable status = timeSeries.getSeriesKey(seriesIndex);

            for (int xyItemIndex = 0, itemCount = timeSeries.getItemCount(seriesIndex); xyItemIndex < itemCount; ++xyItemIndex)
            {
                TimeSeriesDataItem dataItem = timeSeries.getSeries(seriesIndex).getDataItem(xyItemIndex);
                Number periodInMillis = timeSeries.getX(seriesIndex, xyItemIndex);

                AverageStatusDataRow averageStatusDataRow = dataRowMap.containsKey(periodInMillis)
                        ? dataRowMap.get(periodInMillis)
                        : new AverageStatusDataRow(timePeriodUtils.prettyPrint(dataItem.getPeriod()));

                dataRowMap.put(periodInMillis, averageStatusDataRow);

                Number value = dataset.getY(seriesIndex, xyItemIndex);
                String url = urlGenerator.generateURL(dataset, seriesIndex, xyItemIndex);

                averageStatusDataRow.setStatusValue(status.toString(), value.doubleValue(), url);
            }
        }

        return new ArrayList<AverageStatusDataRow>(dataRowMap.values());
    }

    @XmlRootElement
    public static class AverageStatusDataRow
    {
        @XmlElement
        String period;

        @XmlElement
        List<StatusValue> statusValues;

        public AverageStatusDataRow()
        {
            this(null);
        }

        public AverageStatusDataRow(String period)
        {
            this.period = period;
            statusValues = new ArrayList<StatusValue>();
        }

        public void setStatusValue(String status, double value, String url)
        {
            statusValues.add(new StatusValue(status, value, url));
        }
    }

    @XmlRootElement
    public static class StatusValue
    {
        @XmlElement
        String status;

        @XmlElement
        double value;

        @XmlElement
        String url;

        public StatusValue()
        {
            this(null, 0, null);
        }

        public StatusValue(String status, double count, String url)
        {
            this.status = status;
            this.value = count;
            this.url = url;
        }
    }



    /**
     * Looking at https://atlaseye.atlassian.com/changelog/jira?cs=98563, it seems that iGoogle
     * may send selected values of a multiselect as a comma-delimited string instead of multiple params with the same name.
     * Fortunately, status IDs do not contain commas. So if the values specified to this method contains it, we are
     * really sure that iGoogle is trying to make us suck.
     *
     * @return
     * A set of status IDs
     */
    Set<String> splitStatusIdsAsRequired(Collection<String> statusIds)
    {
        if (null == statusIds || statusIds.isEmpty())
        {
            return Collections.emptySet();
        }
        else
        {
            Set<String> newStatusIds = new HashSet<String>();
            for (String statusId : statusIds)
            {
                if (statusId.indexOf(',') >= 0)
                    newStatusIds.addAll(Arrays.asList(StringUtils.split(statusId, ',')));
                else
                    newStatusIds.add(statusId);
            }

            return newStatusIds;
        }
    }

}
