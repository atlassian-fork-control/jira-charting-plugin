package com.atlassian.jira.ext.charting.field;

import com.atlassian.jira.issue.ActionConstants;
import org.ofbiz.core.entity.DelegatorInterface;

public class DateOfFirstResponseDAO extends AbstractSingleDateDAO
{
    private final String selectStatement;

    private static final String ACTION_TABLE_ALIAS = "act";
    private static final String ISSUE_TABLE_ALIAS = "it";

    public DateOfFirstResponseDAO(DelegatorInterface delegatorInterface)
    {
        super(delegatorInterface);
        String actionTable = getTableName("Action");
        String issueTable = getTableName("Issue");
        String action_created = getColName("Action", "created");
        String action_issueid = getColName("Action", "issue");
        String action_type = getColName("Action", "type");
        String action_reporter = getColName("Action", "author");
        String issue_reporter = getColName("Issue", "reporter");
        String issue_id = getColName("Issue", "id");

        selectStatement = "select min( " + ACTION_TABLE_ALIAS + "." + action_created + ") " +
                "from " + actionTable + " " + ACTION_TABLE_ALIAS + " , " + issueTable + " " + ISSUE_TABLE_ALIAS +
                " where " + ACTION_TABLE_ALIAS + "." + action_issueid + " = ? " +
                " and " + ACTION_TABLE_ALIAS + "." + action_issueid + " = " + ISSUE_TABLE_ALIAS + "." + issue_id +
                " and " + ACTION_TABLE_ALIAS + "." + action_type + " = '" + ActionConstants.TYPE_COMMENT + "' " +
                " and " + ISSUE_TABLE_ALIAS + "." + issue_reporter + " != " + ACTION_TABLE_ALIAS + "." + action_reporter;
                                             
    }

    protected String getSelectStatement()
    {
        return selectStatement;
    }
}
