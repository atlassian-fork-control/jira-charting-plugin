package com.atlassian.jira.ext.charting.gadgets.charts;

import com.atlassian.core.util.DateUtils;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.charts.Chart;
import com.atlassian.jira.charts.ChartFactory;
import com.atlassian.jira.charts.jfreechart.ChartHelper;
import com.atlassian.jira.charts.jfreechart.util.ChartUtil;
import com.atlassian.jira.charts.util.DataUtils;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.ext.charting.data.AverageTimeInStatusHitCollector;
import com.atlassian.jira.ext.charting.field.TimeInStatusCFType;
import com.atlassian.jira.ext.charting.field.util.CustomFieldLocator;
import com.atlassian.jira.ext.charting.jfreechart.ChartUtils;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchQuery;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.statistics.DatePeriodStatisticsMapper;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;

import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.urls.XYURLGenerator;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


public class AverageStatusChart implements ChartParamKeys
{
    private final CustomFieldManager customFieldManager;
    private final ConstantsManager constantsManager;
    private final SearchProvider searchProvider;
    private final SearchService searchService;
    private final TimeZoneManager timeZoneManager;
    private final VelocityRequestContextFactory velocityRequestContextFactory;

    public AverageStatusChart(CustomFieldManager customFieldManager, ConstantsManager constantsManager,
            SearchProvider searchProvider, SearchService searchService, TimeZoneManager timeZoneManager,
            VelocityRequestContextFactory velocityRequestContextFactory)
    {
        this.customFieldManager = customFieldManager;
        this.constantsManager = constantsManager;
        this.searchProvider = searchProvider;
        this.searchService = searchService;
        this.timeZoneManager = timeZoneManager;
        this.velocityRequestContextFactory = velocityRequestContextFactory;
    }

    private Map<String, Status> getStatusIdToStatusMap()
    {
        Map<String, Status> statusIdToStatusMap = getLinkedHashMap();

        for (Status statusObj : constantsManager.getStatusObjects())
            statusIdToStatusMap.put(statusObj.getId(), statusObj);

        return statusIdToStatusMap;
    }


// Getter/Setter method don't need to be Clover's calculation
///CLOVER:OFF
    protected LinkedHashMap<String, Status> getLinkedHashMap()
    {
        return new LinkedHashMap<String, Status>();
    }

    private double getDouble(Map<RegularTimePeriod, Double> totalTimes, RegularTimePeriod cursor)
    {
        return totalTimes.containsKey(cursor) ? totalTimes.get(cursor) : 0d;
    }
///CLOVER:ON

    Chart generate(final JiraAuthenticationContext jiraAuthenticationContext, final SearchRequest searchRequest,
            final Set<String> statusIds, final ChartFactory.PeriodName periodName, final int daysPrevious,
            final ChartFactory.PeriodName yAxisPeriod, final int width, final int height, final String xLabel,
            final String tooltip) throws SearchException, IOException
    {
        return generateInternal(jiraAuthenticationContext, searchRequest, statusIds, periodName, daysPrevious,
                yAxisPeriod, width, height, xLabel, tooltip, false);
    }

    Chart generateInline(final JiraAuthenticationContext jiraAuthenticationContext, final SearchRequest searchRequest,
            final Set<String> statusIds, final ChartFactory.PeriodName periodName, final int daysPrevious,
            final ChartFactory.PeriodName yAxisPeriod, final int width, final int height, final String xLabel,
            final String tooltip) throws SearchException, IOException
    {
        return generateInternal(jiraAuthenticationContext, searchRequest, statusIds, periodName, daysPrevious,
                yAxisPeriod, width, height, xLabel, tooltip, true);
    }

    Chart generateInternal(final JiraAuthenticationContext jiraAuthenticationContext,
                   final SearchRequest searchRequest,
                   final Set<String> statusIds,
                   final ChartFactory.PeriodName periodName,
                   final int daysPrevious,
                   final ChartFactory.PeriodName yAxisPeriod,
                   final int width,
                   final int height,
                   final String xLabel,
                   final String tooltip,
                   final boolean inline)
            throws SearchException, IOException
    {
        String timeInStatusFieldId = getTimeInStatusCustomField().getId();
        Map<String, Object> chartParams = new HashMap<String, Object>();
        int normalizedDaysPrevious = calculateNormalizedDaysValue(periodName, daysPrevious);

        Map<String, Status> statusIdToStatusMap = getStatusIdToStatusMap();

        Map<String, Map<RegularTimePeriod, Double>> nameSeriesMap = new HashMap<String, Map<RegularTimePeriod, Double>>();
        final SearchRequest searchRequestCopy = getSearchRequest(searchRequest);

        for (String statusIdString : statusIds)
        {
            Map<RegularTimePeriod, Double> averageInStatusMap = getAverageInStatusMap(statusIdString, searchRequestCopy, jiraAuthenticationContext.getUser(), periodName, null != yAxisPeriod ? yAxisPeriod.toString() : null, normalizedDaysPrevious, timeInStatusFieldId);
            String statusName = statusIdToStatusMap.containsKey(statusIdString) ? statusIdToStatusMap.get(statusIdString).getName() : statusIdString;

            nameSeriesMap.put(statusName, averageInStatusMap);
        }

        XYDataset xyDataset = ChartUtils.generateTimeSeriesXYDataset(nameSeriesMap, timeZoneManager.getLoggedInUserTimeZone());
        ChartHelper helper = getChartHelper(xLabel, xyDataset);

        setChartDefaults(jiraAuthenticationContext, helper);

        XYPlot plot = (XYPlot) helper.getChart().getPlot();
        XYItemRenderer renderer = plot.getRenderer();

        // Old chart look and feel, after the default chart look and feel applied
        if (renderer instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer xyLineAndShapeRenderer = (XYLineAndShapeRenderer) renderer;
            xyLineAndShapeRenderer.setBaseShapesVisible(true);
            xyLineAndShapeRenderer.setBaseShapesFilled(true);
        }

        renderer.setToolTipGenerator(new StandardXYToolTipGenerator("{0} {2} " + tooltip, NumberFormat.getNumberInstance(), NumberFormat.getNumberInstance()));

        final VelocityRequestContext velocityRequestContext = velocityRequestContextFactory.getJiraVelocityRequestContext();
        XYURLGenerator xyurlGenerator = new XYURLGenerator()
        {
            public String generateURL(XYDataset xyDataset, int series, int item)
            {
                TimeSeriesCollection timeSeriesCollection = (TimeSeriesCollection) xyDataset;
                TimeSeries timeSeries = timeSeriesCollection.getSeries(series);
                RegularTimePeriod period = timeSeries.getTimePeriod(item);
                SearchRequest searchUrlSuffix;
                StatisticsMapper createdMapper = new DatePeriodStatisticsMapper(
                        ChartUtil.getTimePeriodClass(periodName),
                        DocumentConstants.ISSUE_UPDATED,
                        timeZoneManager.getLoggedInUserTimeZone());
                searchUrlSuffix = createdMapper.getSearchUrlSuffix(period, searchRequestCopy);

                return velocityRequestContext.getCanonicalBaseUrl() + "/secure/IssueNavigator.jspa?reset=true" + searchService.getQueryString(jiraAuthenticationContext.getLoggedInUser(), searchUrlSuffix.getQuery());
            }
        };
        renderer.setURLGenerator(xyurlGenerator);

        if (inline)
        {
            helper.generateInline(width, height);
        }
        else
        {
            helper.generate(width, height);
        }

        chartParams.put("chart", helper.getLocation());
        chartParams.put(KEY_COMPLETE_DATASET_URL_GENERATOR, xyurlGenerator);
        chartParams.put(KEY_COMPLETE_DATASET, xyDataset);
        chartParams.put("daysPrevious", daysPrevious);
        chartParams.put("period", periodName.toString());
        chartParams.put("imagemap", helper.getImageMap());
        chartParams.put("imagemapName", helper.getImageMapName());

        if (inline)
        {
            String base64Image = ComponentAccessor.getComponent(com.atlassian.jira.charts.util.ChartUtils.class).renderBase64Chart(helper.getImage(), "Average Status Chart");
            chartParams.put("base64Image", base64Image);
        }

        return new Chart(helper.getLocation(), helper.getImageMap(), helper.getImageMapName(), chartParams);
    }

// Setter/getter need not to be calculated
///CLOVER:OFF
    protected void setChartDefaults(
            final JiraAuthenticationContext jiraAuthenticationContext,
            ChartHelper helper)
    {
        ChartUtil.setDefaults(helper.getChart(), jiraAuthenticationContext.getI18nHelper());
    }

    protected ChartHelper getChartHelper(final String xLabel,
            XYDataset xyDataset)
    {
        return ChartUtils.generateMultiLineChart(xyDataset, null, null, xLabel, null);
    }

    protected SearchRequest getSearchRequest(final SearchRequest searchRequest)
    {
        return new SearchRequest(searchRequest);
    }

    protected int calculateNormalizedDaysValue(
            final ChartFactory.PeriodName periodName, final int daysPrevious)
    {
        return DataUtils.normalizeDaysValue(daysPrevious, periodName);
    }

    protected CustomField getTimeInStatusCustomField()
    {
        return CustomFieldLocator.getCustomField(customFieldManager, TimeInStatusCFType.class);
    }
///CLOVER:ON

    private Map<RegularTimePeriod, Double> getAverageInStatusMap(String status, SearchRequest sr, ApplicationUser remoteUser, ChartFactory.PeriodName periodName, String yAxisPeriod, int days, String averageTimeInStatusFieldId)
            throws IOException, SearchException
    {
        days = calculateNormalizedDaysValue(periodName, days);

        final Map<RegularTimePeriod, Double> timeInStatusTotals = new TreeMap<RegularTimePeriod, Double>();
        final Map<RegularTimePeriod, Double> timeInStatusCounts = new TreeMap<RegularTimePeriod, Double>();

        final Class timePeriodClass = ChartUtil.getTimePeriodClass(periodName);

        searchProvider.search(SearchQuery.create(sr.getQuery(), remoteUser),
                new AverageTimeInStatusHitCollector(DocumentConstants.ISSUE_RESOLUTION_DATE, averageTimeInStatusFieldId,
                        status, timeInStatusTotals, timeInStatusCounts,
                        timePeriodClass, days - 1,
                        yAxisPeriod == null, timeZoneManager.getLoggedInUserTimeZone()));

        Map<RegularTimePeriod, Double> averageTimeInStatus = new HashMap<RegularTimePeriod, Double>();
        for (RegularTimePeriod period : timeInStatusTotals.keySet())
        {
            double total = getDouble(timeInStatusTotals, period);
            double count = getDouble(timeInStatusCounts, period);

            double average = 0D;

            if (count > 0D)
            {
                average = total / count;
            }

            double yAxisPeriodLong = 1D;
            if (yAxisPeriod != null)
            {
                yAxisPeriodLong = "daily".equalsIgnoreCase(yAxisPeriod) ? DateUtils.DAY_MILLIS : DateUtils.HOUR_MILLIS;
            }
            averageTimeInStatus.put(period, average / yAxisPeriodLong);
        }

        return Collections.unmodifiableMap(averageTimeInStatus);
    }
}
