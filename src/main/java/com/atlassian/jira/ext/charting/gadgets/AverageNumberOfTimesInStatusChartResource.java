package com.atlassian.jira.ext.charting.gadgets;

import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.charts.Chart;
import com.atlassian.jira.charts.ChartFactory;
import com.atlassian.jira.charts.util.ChartUtils;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.ext.charting.gadgets.charts.AverageNumberOfTimesInStatusChart;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.rest.v1.model.errors.ErrorCollection;
import com.atlassian.jira.rest.v1.model.errors.ValidationError;
import com.atlassian.jira.rest.v1.util.CacheControl;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.util.OutlookDateManager;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import org.apache.commons.httpclient.HttpStatus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("numberoftimesinstatus")
@AnonymousAllowed
@Produces({MediaType.APPLICATION_JSON})
public class AverageNumberOfTimesInStatusChartResource extends AverageStatusChartResource
{
    private final TimeZoneManager timeZoneManager;

    public AverageNumberOfTimesInStatusChartResource(JiraAuthenticationContext jiraAuthenticationContext, ProjectManager projectManager, SearchRequestService searchRequestService, SearchService searchService, ChartUtils chartUtils, PermissionManager permissionManager, VelocityRequestContextFactory velocityRequestContextFactory, ApplicationProperties applicationProperties, TimeZoneManager timeZoneManager, CustomFieldManager customFieldManager, ConstantsManager constantsManager, IssueIndexManager issueIndexManager, SearchProvider searchProvider)
    {
        super(jiraAuthenticationContext, projectManager, searchRequestService, searchService, chartUtils, permissionManager, velocityRequestContextFactory, applicationProperties, customFieldManager, constantsManager, issueIndexManager, searchProvider, timeZoneManager);
        this.timeZoneManager = timeZoneManager;
    }

    @GET
    @Path("config/validate")
    @Produces({MediaType.APPLICATION_JSON})
    public Response validate(
            @QueryParam("projectOrFilterId") String projectOrFilterId,
            @QueryParam("daysprevious") String daysPrevious)
    {
        Collection<ValidationError> validationErrors = new ArrayList<ValidationError>();

        validateProjectOrFilterId(projectOrFilterId, validationErrors);
        validateDaysPrevious(daysPrevious, validationErrors);


        if (validationErrors.isEmpty())
        {
            return Response.ok().cacheControl(CacheControl.NO_CACHE).build();
        }
        else
        {
            return Response.status(HttpStatus.SC_BAD_REQUEST).entity(ErrorCollection.Builder.newBuilder(validationErrors).build()).cacheControl(CacheControl.NO_CACHE).build();
        }
    }

    @GET
    @Path("generate")
    @Produces({MediaType.APPLICATION_JSON})
    public Response generate(
            @QueryParam("projectOrFilterId") String projectOrFilterId,
            @QueryParam("statuses") List<String> statuses,
            @QueryParam("periodName") String periodName,
            @QueryParam("daysprevious") int daysprevious,
            @QueryParam("width") @DefaultValue("400") int width,
            @QueryParam("height") @DefaultValue("250") int height,
            @QueryParam("returnData") @DefaultValue("false") boolean returnData,
            @QueryParam (INLINE) @DefaultValue ("false") final boolean inline) throws SearchException, IOException
    {

        Map<String, Object> params = new HashMap<String, Object>();

        SearchRequest searchRequest = new SearchRequest(chartUtils.retrieveOrMakeSearchRequest(projectOrFilterId, params));
        AverageNumberOfTimesInStatusChart chart = new AverageNumberOfTimesInStatusChart(
                customFieldManager, constantsManager, searchProvider, searchService, timeZoneManager, velocityRequestContextFactory);

        try
        {
            Chart theChart = inline ? chart.generateInline(
                    jiraAuthenticationContext, searchRequest, splitStatusIdsAsRequired(statuses),
                    ChartFactory.PeriodName.valueOf(periodName),
                    daysprevious,
                    width, height
            ) : chart.generate(
                    jiraAuthenticationContext, searchRequest, splitStatusIdsAsRequired(statuses),
                    ChartFactory.PeriodName.valueOf(periodName),
                    daysprevious,
                    width, height
            );

            params.putAll(theChart.getParameters());

            AverageStatusChartResource.AverageStatusChart averageStatusChart = new AverageStatusChartResource.AverageStatusChart(
                    theChart.getLocation(),
                    theChart.getImageMap(),
                    theChart.getImageMapName(),
                    width,
                    height,
                    getProjectNameOrFilterTitle(projectOrFilterId),
                    getFilterUrl(params),
                    String.valueOf(daysprevious),
                    theChart.getBase64Image()
            );

            if (returnData)
            {
                averageStatusChart.selectedStatusNames = getSelectedStatuses(theChart.getParameters());
                averageStatusChart.data = getData(theChart.getParameters());
            }

            return Response.ok(averageStatusChart).cacheControl(CacheControl.NO_CACHE).build();
        }
        catch (IllegalStateException ise)
        {
            return createServiceUnavailableErrorMessageResponse(jiraAuthenticationContext.getI18nHelper().getText("portlet.numberinstatus.noissues"));
        }
    }
}
