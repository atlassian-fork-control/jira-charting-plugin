package com.atlassian.jira.ext.charting.searchers;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.issue.customfields.SortableCustomFieldSearcher;
import com.atlassian.jira.issue.customfields.searchers.DateTimeRangeSearcher;
import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.LuceneFieldSorter;
import com.atlassian.jira.issue.statistics.DateFieldSorter;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.util.JqlDateSupport;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.action.util.CalendarLanguageUtil;

/**
 * This class is here because JIRA may have problems loading built-in searchers in v2 plugin context.
 */
public class FirstResponseDateSearcher extends DateTimeRangeSearcher implements SortableCustomFieldSearcher
{
    public FirstResponseDateSearcher(JiraAuthenticationContext context, JqlOperandResolver jqlOperandResolver, VelocityRequestContextFactory velocityRenderContext, ApplicationProperties applicationProperties, VelocityTemplatingEngine velocityManager, CalendarLanguageUtil calendarUtils, JqlDateSupport dateSupport, CustomFieldInputHelper customFieldInputHelper, TimeZoneManager timeZoneManager, DateTimeFormatterFactory dateTimeFormatterFactory)
    {
        super(context, jqlOperandResolver, velocityRenderContext, applicationProperties, velocityManager, calendarUtils, dateSupport, customFieldInputHelper, timeZoneManager, dateTimeFormatterFactory);
    }

    @Override
    public LuceneFieldSorter getSorter(CustomField field)
    {
        return new DateFieldSorter(field.getId());
    }
}
